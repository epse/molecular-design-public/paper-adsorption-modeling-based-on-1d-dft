#%% 
# ©2024 ETH Zurich, Fabian Mayer; D-MAVT; Energy and Process Systems Engineering

# This script calculates isotherms based on pressures and isotherm parameters in a csv-file.
# It can be specified if the isotherms should be calculated for the parameters of a fit of the same fluid and temperature or extrapolated from a fit of another fluid or another temperature or both.

#%% Import packages
import pandas as pd
import numpy as np

from feos.si import *

from input_processing import prisma_4temp_csv_to_input_isotherm
from calc_DFT_isotherm import calc_DFT_isotherms
from calc_langmuir_isotherm import calc_langmuir_isotherms
from calc_toth_isotherm import calc_toth_isotherms
from calc_sips_isotherm import calc_sips_isotherms
from error_function import error_function

#%% Input

path_input_file = "results/xxx.csv"


model = "DFT"  # langmuir or DFT or toth or sips

# specify geometry of the pore in the DFT calculation or if it should be read from input file
geometry = "read" # "read" (read from input file for each isotherm) or "Cylindrical" or "Spherical" or "Cartisian"

# number of isotherms per batch. batch means a group of isotherms of the same adsorbent and the fluids and temperatures that should be considered for the extrapolation.
# E.g. equals 1 if each isotherm should be calculated with the parameters of the fit of the same fluid and same temperature
#      equals 2 for fluid extrapolation of 2 fluids at 1 temperature
#      equals 4 for temperature extrapolation of 1 fluid at 4 temperatures
#      equals 8 for fluid and temperature extrapolation of 2 fluids at 4 temperatures
n_isotherms_per_batch = 5

# specify for each isotherm in the batch the parameters from which fit of isotherms within the batch should be used
base_isotherms = [0, 0, 0, 0, 0, 0]
#base_isotherms = [0]

# only relevant for file name of results file
extrapolation_method = "DFTisotherms" #"DFTisotherms" #"fluidextrapolation" #"tempextrapolation"

# specify which error metrics should be calculated. See file error_function.py for available error metrics.
error_metrics = ['RMSE', 'SSE', 'SAE', 'ARE', 'MARD', 'MRD', 'difference_error', 'MAD', 'MARD_max_loading', 'difference_max_loading', 'MAD_max_loading', 'initial_slope_error', 'cv', 'cv_rmse']
#%% End Input


#%% Check input for errors
if np.size(base_isotherms) != n_isotherms_per_batch:
    raise ValueError("base_isotherms must have the same length as n_isotherms_per_batch")
if np.max(base_isotherms) >= n_isotherms_per_batch:
    raise ValueError("entries in base_isotherms must be smaller than n_isotherms_per_batch")

#%% Read input and setup results file

# read input isotherms from csv file
input_isotherms, input_data = prisma_4temp_csv_to_input_isotherm(path_input_file, column_name_loadings='Uptake [mol/kg]')

input_data[f'{model}_loadings_in_mol/kg'] = [np.array([]) for i in range(len(input_data))]
input_data['extrapolated?'] = [np.array([]) for i in range(len(input_data))]

for error_metric in error_metrics:
    input_data[error_metric] = [np.array([]) for i in range(len(input_data))]

extrapolated_i=[]
for i in range(0, n_isotherms_per_batch):
    if i==base_isotherms[i]:
        extrapolated_i.append(False)
    else:
        extrapolated_i.append(True)

#%% Calculate isotherms and errors
# go through input_isotherms by batch
for batch_i in range(0,input_data.shape[0]//n_isotherms_per_batch):

    print(batch_i)

    for isotherm_i in range(0, n_isotherms_per_batch):
        # calculate isotherms
        x = np.array(np.matrix(input_data.at[batch_i*n_isotherms_per_batch+base_isotherms[isotherm_i], f'{model}_parameters_fit'])).ravel()
        input_isotherm_i = [input_isotherms[batch_i*n_isotherms_per_batch+isotherm_i]]
        if model == "DFT":
            if geometry == "read":
                geometry_i = input_data.at[batch_i*n_isotherms_per_batch+base_isotherms[isotherm_i], 'geometry']
            else:
                geometry_i = geometry
            isotherm = calc_DFT_isotherms(x, input_isotherm_i, geometry_i)
        elif model in ["langmuir", "toth", "sips"]:
            isotherm = eval(f'calc_{model}_isotherms')(x, input_isotherm_i)
        else:
            raise ValueError(f"model {model} not implemented")
        loadings = isotherm[0].loadings
            
        errors = []
        for error_metric in error_metrics:
            #error_total_j, error_j = error_function(input_isotherm_i, isotherm, error_metric, skipnans = True)
            error_total_j, error_j = error_function(input_isotherm_i, isotherm, error_metric, skipnans = False)
            errors.append(error_total_j)


        # add results to input_data
        input_data.at[batch_i*n_isotherms_per_batch+isotherm_i, f'{model}_loadings_in_mol/kg'] = loadings / (MOL/KILOGRAM)
        input_data.at[batch_i*n_isotherms_per_batch+isotherm_i, 'extrapolated?'] = extrapolated_i[isotherm_i]
        for j, error_metric in enumerate(error_metrics):
            input_data.at[batch_i*n_isotherms_per_batch+isotherm_i, error_metric] = errors[j]
            


    input_data.to_csv(f'{path_input_file[0:-4]}_{extrapolation_method}.csv')


