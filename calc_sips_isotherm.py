# ©2024 ETH Zurich, Fabian Mayer; D-MAVT; Energy and Process Systems Engineering

from feos.si import *

from sips_model import sips_isotherm


def calc_sips_isotherms(x, input_isotherms):
    """
    calculates sips isotherms based on fluid and temperature in input isotherms and unscaled x vector with variables from fitting

    Parameters
    ----------
    x : [float]
        unscaled variable vector in the order C, b_0, Q, n_0, alpha
        Sips isotherms are calculated for this parameterset of the adsorbent
    input_isotherms : [input_isotherm]
        array of input_isotherm objects with all information of experimental or simulated isotherms
        temperatures are taken from this array to calculate Sips isotherms

    Returns
    -------
    sips_isotherms : [sips_isotherm]
        array of sips_isotherm objects with all informations of calculated Sips isotherms
    """

    sips_isotherms = []
    for input_isotherm_i in input_isotherms:

        T = input_isotherm_i.temperature
        C = x[0] * MOL/KILOGRAM
        b_0 = x[1] * 1/BAR
        Q = x[2] * JOULE/MOL
        n_0 = x[3]
        alpha = x[4]
        
        pressures = input_isotherm_i.pressures

        sips_isotherm_i = sips_isotherm(C, b_0, Q, n_0, alpha)
        sips_isotherm_i.add_isotherm_values(pressures, T)
        sips_isotherms.append(sips_isotherm_i)
        
                
    return sips_isotherms