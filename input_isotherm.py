# ©2024 ETH Zurich, Fabian Mayer; D-MAVT; Energy and Process Systems Engineering

import numpy as np
from feos.si import *

class input_isotherm:
    """
    A class used to represent an isotherm from a input file, e.g. from experimental data or simulated data
    
    Attributes
    ----------

    Methods
    -------

    """

    def __init__(self, adsorbent, adsorbate, adsorbate_short, temperature, pressures, loadings, type_IUPAC = 1, excess = False):
        """
        Parameters
        ----------
        adsorbent : str
            The name of the adsorbent
        adsorbate : str
            The name of the adsorbate
        adsorbate_short : str
            The molecular formula of the adsorbate
        temperature : SI
            Temperature of isotherm
        pressures : SI float
            array of pressures
        loadings : SI float
            array of loadings
        type_IUPAC : int, opt
            specifies type of isotherm according to IUPAC classification (default = 1)
        excess : bolean, optional
            specify if loading values are excess loading (True) or absolute loading (False, default)
        """

        self.adsorbent = adsorbent
        self.adsorbate = adsorbate
        self.adsorbate_short = adsorbate_short
        self.temperature = temperature
        self.pressures = pressures
        self.loadings = loadings
        self.type_IUPAC = type_IUPAC
        self.excess = excess
        self.max_loading = np.max(self.loadings/(MOL/KILOGRAM))*MOL/KILOGRAM
