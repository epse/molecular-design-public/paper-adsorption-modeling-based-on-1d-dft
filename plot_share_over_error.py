#%% 
# ©2024 ETH Zurich, Fabian Mayer; D-MAVT; Energy and Process Systems Engineering

# This script plots the share of datapoints below a certain error over the error metric

#%% Import packages
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np


#%% Input


# paths to the csv files containing the error data
path_input_file = "results/xxx.csv"

# name of error metric for which the plot should be created
error_metric = 'MARD'

extrapolation_method = "fluidextrapolation"  # currently only fluidextrapolation implemented


# specify the molecules for which the plot should be created
#molecules = ['N2', 'CH4']
molecules = ['CO2', 'N2']


figsize = (3.2, 2.5)
fontsize = 11

# latex labels for molecules
molecule_labels = {'CO2': r'$\mathrm{CO}_2$', 'N2': r'$\mathrm{N}_2$', 'CH4': r'$\mathrm{CH}_4$'}

# specify the colors for the different molecules
colors_molecules = {'CO2': 'green', 'N2': 'blue', 'CH4': 'red'}

# specify linestyle for the different molecules
linestyles_molecules = {'CO2': '--', 'N2': '-', 'CH4': '--'}

# set x-limit
XLIM = 100




#%% End Input

#%% Input data processing


# read error data from csv files
error_data = pd.read_csv(path_input_file)


# extract column with errors for each molecule
error_data_molecules = {}
for molecule in molecules:
    error_data_molecules[molecule] = []
    for i in range(len(error_data)):
        if error_data['Molecule'][i] == molecule and np.isnan(error_data[error_metric][i]) == False:
            error_data_molecules[molecule].append(error_data[error_metric][i])

# sort lists of error data
for molecule in molecules:
    error_data_molecules[molecule] = sorted(error_data_molecules[molecule])
    


# calculate share of datapoints below a certain error
share_below_error = {}
for molecule in molecules:
    share_below_error[molecule] = []
    for i in range(len(error_data_molecules[molecule])):
        share_below_error[molecule].append((i+1)/len(error_data_molecules[molecule])*100)


#%% Plotting


# set fonts for plots to latex fonts
plt.rcParams.update({"text.usetex": True, "pgf.texsystem": "pdflatex", 'font.family': 'sans-serif', 'font.sans-serif': 'Computer Modern Sans Serif', 'font.size': fontsize})


# create figure
fig, ax = plt.subplots(figsize=figsize)

# plot share of datapoints below a certain error over the error metric
for molecule in molecules:
    ax.plot(error_data_molecules[molecule], share_below_error[molecule], label=molecule_labels[molecule], color=colors_molecules[molecule], linestyle=linestyles_molecules[molecule])

# set x- and y-labels
ax.set_xlabel(f'{error_metric} in \%' if error_metric == 'MARD' else error_metric, fontsize=fontsize)
ax.set_ylabel('share of adsorbents in \%', fontsize=fontsize)

# set x- and y-ticks
ax.tick_params(axis='both', which='major', labelsize=fontsize)
ax.tick_params(axis='both', which='minor', labelsize=fontsize)

# set x- and y-limits
ax.set_xlim(left=0, right = XLIM)
ax.set_ylim(bottom=0, top=100)

# set steps of x-ticks
ax.xaxis.set_major_locator(plt.MultipleLocator(10))

# set steps of y-ticks
#ax.yaxis.set_major_locator(plt.MultipleLocator(10))

# set legend
ax.legend(fontsize=fontsize)

# save figure
fig.savefig(f'{path_input_file[0:-4]}_share_over_{error_metric}.pdf', bbox_inches='tight')

#%% End Plotting



