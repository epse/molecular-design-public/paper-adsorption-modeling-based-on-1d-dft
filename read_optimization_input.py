# ©2024 ETH Zurich, Fabian Mayer; D-MAVT; Energy and Process Systems Engineering

import toml


def scale_var(x_in, path_bounds, unscale = False):
    """
    scale variable vector or remove scaling according to bounds to values between 0 and 1

    Parameters
    ----------
    x_in : [float]
        variable vector to be scaled or unscaled
    unscale : boolean, opt
        determine if variable vector should be scaled (False, default) or unscaled (True)
    
    Raises
    ------
    ValueError
        If variable to be scaled or unscaled is outside of bounds according to bounds of toml-file
    """

    n_head = 2 #number of entries in head of toml file that do not correspond to a variable in variable vector
    bounds = toml.load(path_bounds)
    x_out = []
    for i,b in enumerate(bounds):
        if i<2:
            continue
        if unscale:
            #remove scaling
            if x_in[i-n_head] < 0 or x_in[i-n_head] > 1:
                raise ValueError("Value of x[" + str(i-n_head) + "] out of bounds")
            x_out.append(bounds[b]['lb'] + x_in[i-n_head] * (bounds[b]['ub'] - bounds[b]['lb']))
        else:
            #scale variable vector
            x_out.append((x_in[i-n_head] - bounds[b]['lb']) / (bounds[b]['ub'] - bounds[b]['lb']))
            if x_out[-1] < 0 or x_out[-1] > 1:
                raise ValueError("Value of x[" + str(i-n_head) + "] out of bounds of file " + path_bounds)


    return x_out



def read_initial_values(path_initial_values):
    """read start values for optimization from toml file and scale according to bounds"""
    init = toml.load(path_initial_values)
    x_unscaled = list(init['initial_values'].values())


    return x_unscaled
