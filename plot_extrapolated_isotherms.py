#%% 
# ©2024 ETH Zurich, Fabian Mayer; D-MAVT; Energy and Process Systems Engineering

# This script plots isotherms of one or more csv file containing isotherm data for one or several temperatures.

#%% Import packages
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

from feos.si import *

from input_processing import prisma_4temp_csv_to_input_isotherm

#%% Input

# list of paths to the csv file containing the isotherm data
path_input_file = ["results/xxx:DFT.csv", 
                   "results/xxx_langmuir.csv",
                   "results/xxx_toth.csv"]

# path to the directory where the plots should be saved
dir_results = "results/xxx/isotherms"

# specify file format of the saved plots
file_format = 'pdf'  # 'png' or 'pdf'

# specify the temperatures of isotherms in the csv file
T_in = [298.15, 323.15, 348.15, 373.15, 398.15]
#T_in = [298.15]


# specify the name of the models used to generate the data in the csv files
#model = ["DFT"]  # langmuir or DFT or toth or sips
model = ["DFT", "langmuir", "toth"]
#model = ["toth"]

# specify name of the models that should be used in legend
model_legend = ["1D-DFT", "Langmuir", "Toth"]
#model_legend = ["1D-DFT"]
#model_legend = ["Toth"]

# linestyles of the models
linestyles_model = {'1D-DFT': '-', 'Langmuir': '--', 'Toth': ':', 'Sips': '--'}

# color of GCMC isotherms
color_gcmc = 'black'

# colors of the model isotherms according to molecules
colors_model = {'CO2': ['green', 'orange', 'blue'], 'N2': ['green', 'orange', 'blue'], 'CH4': ['green', 'orange', 'blue'], 'C2H6O':  ['green', 'orange', 'blue']}

# decision variable if the isotherms should be plotted in subplots or in one plot
subplot = True
sharey = True

# figure size
figsize = (7, 1.5) #(tempextrapolation 5 subplots) #(8, 6) #(7, 3)
#figsize = (3.2, 1.5) #(fluidextrapolation 1 subplot) #(8, 6) #(7, 3)


# font size of all fonts in the plot
fontsize = 11 #15

#%% End Input

#%% Input data processing
input_isotherms, input_data = prisma_4temp_csv_to_input_isotherm(path_input_file[0], column_name_loadings='Uptake [mol/kg]')
model_isotherms = []
for i, path in enumerate(path_input_file):
    model_isotherms_i, _ = prisma_4temp_csv_to_input_isotherm(path, column_name_loadings=f'{model[i]}_loadings_in_mol/kg')
    model_isotherms.append(model_isotherms_i)

n_isotherms_per_file = len(T_in)

#go through input_isotherms by adsorbent and fluid
for file_i in range(0,input_data.shape[0]//n_isotherms_per_file):
    print(file_i)


    # create a figure with subplots
    if subplot:
        if sharey:
            nrows = 1
            ncols = n_isotherms_per_file
        else:
            # check if n_isotherms_per_file is a quadratic number
            if np.sqrt(n_isotherms_per_file) % 1 == 0:
                nrows=int(np.sqrt(n_isotherms_per_file))
                ncols=int(np.sqrt(n_isotherms_per_file))
            else:
                nrows=int(np.sqrt(n_isotherms_per_file))
                ncols=int(np.sqrt(n_isotherms_per_file))+1

    else:
        nrows=1
        ncols=1

    # set fonts for plots to latex fonts
    plt.rcParams.update({"text.usetex": True, "pgf.texsystem": "pdflatex", 'font.family': 'sans-serif', 'font.sans-serif': 'Computer Modern Sans Serif', 'font.size': fontsize})

    fig, axs = plt.subplots(nrows=nrows, ncols=ncols, figsize=figsize, squeeze=False, sharey=sharey)    

    axs_flat = axs.flatten()
    # plot data on each subplot
    for T_i, T in enumerate(T_in):
        if subplot:
            index = T_i
        else:
            index = 0

        axs_flat[index].scatter(input_isotherms[file_i*n_isotherms_per_file+T_i].pressures/(BAR), input_isotherms[file_i*n_isotherms_per_file+T_i].loadings/(MOL/(KILO*GRAM)), marker=".", color=color_gcmc, label="GCMC")
        for i_iso, model_isotherms_i in enumerate(model_isotherms):
            axs_flat[index].plot(model_isotherms_i[file_i*n_isotherms_per_file+T_i].pressures/(BAR), model_isotherms_i[file_i*n_isotherms_per_file+T_i].loadings/(MOL/(KILO*GRAM)), linestyle = linestyles_model[model_legend[i_iso]], color=colors_model[input_isotherms[file_i*n_isotherms_per_file+T_i].adsorbate_short][i_iso], label=f"{model_legend[i_iso]}")
       
        if subplot:

            axs_flat[index].set_title(f"{T}\,K", fontsize=fontsize)
            
        if not sharey or index==0:
            axs_flat[index].set_ylabel("loading in mol/kg", fontsize=fontsize)
        if not sharey or index==int(n_isotherms_per_file/2):
            axs_flat[index].set_xlabel("pressure in bar", fontsize=fontsize)
        #adjust fontsize of ticks
        axs_flat[index].tick_params(axis='both', which='major', labelsize=fontsize-3)
        axs_flat[index].tick_params(axis='both', which='minor', labelsize=fontsize-3)
        #axs[T_i//2, T_i%2].set_xscale('log')
        #axs[T_i//2, T_i%2].set_yscale('log')

    


    # don't show the last subplot if there is no data for it, instead show legend there
    if np.sqrt(n_isotherms_per_file) % 1 != 0 and subplot and n_isotherms_per_file % 2 == 1 and not sharey:
        axs_flat[-1].axis('off')
        handles, labels = axs_flat[0].get_legend_handles_labels()
        axs_flat[-1].legend(handles, labels, loc='center left', fontsize=fontsize)

    else:
        # Add a single legend for the figure
        handles, labels = axs_flat[0].get_legend_handles_labels()
        #fig.legend(handles, labels, loc='lower center', ncol=4, bbox_to_anchor=(0.5, -0.5))
        fig.legend(handles, labels, loc='lower center', ncol=4, bbox_to_anchor=(0.5, -0.4))

    # adjust the layout and padding between subplots
    if sharey:
        # remove spacing between subplots
        fig.subplots_adjust(wspace=0)
    else:
        fig.tight_layout(pad=2)

    

    # Save figure
    fig.savefig(f"{dir_results}/{input_data['MOF'][file_i*n_isotherms_per_file]}_{input_isotherms[file_i*n_isotherms_per_file+T_i].adsorbate_short}.{file_format}", bbox_inches='tight')
        
    plt.close()



   


    