# ©2024 ETH Zurich, Fabian Mayer; D-MAVT; Energy and Process Systems Engineering

from feos.si import *

import numpy as np
from sklearn.linear_model import LinearRegression


def error_function(input_isotherms, model_isotherms, error_metric = "RMSE", skipnans = False):
    """
    calculate error between isotherms from experimental or simulated data and isotherms calculated by a model (e.g. DFT or Langmuir model)

    Parameters
    ----------
    input_isotherms : [input_isotherm]
        isotherms from experimental or simulated data
    model_isotherms : [DFT_isotherm or langmuir_isotherm]
        isotherms calculated by DFT or Langmuir model
    error_metric : str, opt
        specify which metric should be used to calculate the error
    skipnans : boolean
        if True and at least one loading of the DFT isotherm is NaN, the error calculation is not performed and the error is set to NaN TODO: improve NaN handling

    Returns
    -------
    error_total : float
        value of error metric between isotherms from experimental or simulated data and DFT-calculated isotherms
    errors : [float]
        array with error metric for each isotherm
    """

    
    errors = []
    for i, isotherm_i in enumerate(input_isotherms):  

        if skipnans and np.isnan(model_isotherms[i].loadings/(MOL/KILOGRAM)).any():
            errors.append(np.NaN)
        else:

            match error_metric:
                case "RMSE":
                    # root mean squared error
                    errors.append(np.sqrt(np.nanmean(np.square(isotherm_i.loadings/(MOL/KILOGRAM) - model_isotherms[i].loadings/(MOL/KILOGRAM)))))
                case "SSE":
                    # sum of squared errors
                    errors.append(np.sum(np.square(isotherm_i.loadings/(MOL/KILOGRAM) - model_isotherms[i].loadings/(MOL/KILOGRAM))))
                case "SAE":
                    # sum of absolute errors
                    errors.append(np.sum(np.absolute(model_isotherms[i].loadings/(MOL/KILOGRAM) - isotherm_i.loadings/(MOL/KILOGRAM))))
                case "ARE":
                    # average relative error
                    errors.append(100/(np.size(isotherm_i.loadings/(MOL/KILOGRAM)) * np.sum(np.absolute((isotherm_i.loadings/(MOL/KILOGRAM) - model_isotherms[i].loadings/(MOL/KILOGRAM))/(isotherm_i.loadings/(MOL/KILOGRAM))))))
                case "MARD":
                    # mean absolute relative deviation in percentage
                    errors.append(np.nanmean(np.absolute((isotherm_i.loadings/(MOL/KILOGRAM) - model_isotherms[i].loadings/(MOL/KILOGRAM))/(isotherm_i.loadings/(MOL/KILOGRAM)) * 100)))
                case "MRD":
                    # mean relative deviation in percentage
                    errors.append(np.nanmean((model_isotherms[i].loadings/(MOL/KILOGRAM) - isotherm_i.loadings/(MOL/KILOGRAM))/(isotherm_i.loadings/(MOL/KILOGRAM)) * 100))
                case "difference_error":
                    # averaged error
                    errors.append(np.nanmean(( model_isotherms[i].loadings/(MOL/KILOGRAM) - isotherm_i.loadings/(MOL/KILOGRAM))))
                case "MAD":
                    # mean absolute deviation
                    errors.append(np.nanmean(np.absolute(isotherm_i.loadings/(MOL/KILOGRAM) - model_isotherms[i].loadings/(MOL/KILOGRAM))))
                case "MARD_max_loading":
                    # mean absolute relative deviation of 5 entries with maximum pressure in percentage
                    errors.append(np.nanmean(np.absolute(((isotherm_i.loadings/(MOL/KILOGRAM))[-5:] - (model_isotherms[i].loadings/(MOL/KILOGRAM))[-5:])/((isotherm_i.loadings/(MOL/KILOGRAM))[-5:]) * 100)))
                case "difference_max_loading":
                    # averaged error of 5 entries with maximum pressure
                    errors.append(np.nanmean((((model_isotherms[i].loadings/(MOL/KILOGRAM))[-5:] - (isotherm_i.loadings/(MOL/KILOGRAM))[-5:]))))
                case "MAD_max_loading":
                    # mean absolute deviation of 5 entries with maximum pressure
                    errors = []
                    for i, isotherm_i in enumerate(input_isotherms):  
                        errors.append(np.nanmean(np.absolute((((isotherm_i.loadings/(MOL/KILOGRAM))[-5:] - (model_isotherms[i].loadings/(MOL/KILOGRAM))[-5:])))))
                case "initial_slope_error":
                    # difference in slope of 5 first entries
                    model = LinearRegression()
                    y = (model_isotherms[i].loadings/(MOL/KILOGRAM))[:5]
                    x = (model_isotherms[i].pressures/(KILO*PASCAL))[:5].reshape((-1,1))
                    if np.isnan(x).any() or np.isnan(y).any():
                        errors.append(np.NaN)
                    else:
                        model.fit(x,y)
                        initial_slope_DFT = model.coef_[0]
    
                        model = LinearRegression()
                        y = (isotherm_i.loadings/(MOL/KILOGRAM))[:5]
                        x = (isotherm_i.pressures/(KILO*PASCAL))[:5].reshape((-1,1))
                        model.fit(x,y)
                        initial_slope_input = model.coef_[0]
                        errors.append((initial_slope_DFT - initial_slope_input)/initial_slope_input*100)
                case "cv":
                    # coefficient of variation
                    errors.append(np.std(np.absolute(isotherm_i.loadings/(MOL/KILOGRAM) - model_isotherms[i].loadings/(MOL/KILOGRAM)))/np.mean(np.absolute(isotherm_i.loadings/(MOL/KILOGRAM) - model_isotherms[i].loadings/(MOL/KILOGRAM))))
                case "cv_rmse":
                    # coefficient of variation calculated by definition of normalized rmse
                    errors.append(np.sqrt(np.nanmean(np.square(isotherm_i.loadings/(MOL/KILOGRAM) - model_isotherms[i].loadings/(MOL/KILOGRAM))))/np.nanmean(np.absolute(isotherm_i.loadings/(MOL/KILOGRAM) - model_isotherms[i].loadings/(MOL/KILOGRAM))))
                case _:
                    raise ValueError("error metric " + error_metric +" not available!")
            
    error_total = np.nanmean(errors)
    
    return error_total, errors

    
