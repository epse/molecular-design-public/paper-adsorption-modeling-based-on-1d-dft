# ©2024 ETH Zurich, Fabian Mayer; D-MAVT; Energy and Process Systems Engineering

from feos.si import *

import numpy as np


class langmuir_isotherm:
    """
    A class used to calculate an isotherm using the temperature-dependent Langmuir model
    
    Attributes
    ----------

    Methods
    -------

    """
    def __init__(self, C, b_0, Q):
        """
        Initialize Langmuir isotherm object
        Parameters
        ----------
        C : float
            parameter C of langmuir model (maximum loading, usually in mol/kg)
        b_0 : float
            parameter b_0 of langmuir model (describes the slope of the isotherm, usually in 1/bar)
        Q : float
            parameter Q of langmuir model (describes the temperature-dependence of the isotherm, usually in J/mol)
        """
        self.C = C
        self.b_0 = b_0
        self.Q = Q

    
    def calc_loading(self):
        """
        Calculate loadings of Langmuir isotherm object
        Parameters
        ----------
        Returns
        -------
        loadings : [SI]
            loadings of Langmuir isotherm object
        """

        b = self.b_0 * np.exp(self.Q/(RGAS*self.temperature))

        loadings = self.C * b * self.pressures /(1+b*self.pressures)

        return loadings


    def add_isotherm_values(self, pressures, temperature):
        """
        Add pressures, temperature, and loadings to Langmuir isotherm object
        Parameters
        ----------
        pressures : [SI]
            pressure vector for isotherm calculation
        temperature : SI
            Temperature of isotherm
        """
        self.temperature = temperature
        self.pressures = pressures
        self.loadings = self.calc_loading()
        

        