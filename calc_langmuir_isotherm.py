# ©2024 ETH Zurich, Fabian Mayer; D-MAVT; Energy and Process Systems Engineering

from feos.si import *

from langmuir_model import langmuir_isotherm


def calc_langmuir_isotherms(x, input_isotherms):
    """
    calculates langmuir isotherms based on fluid and temperature in input isotherms and unscaled x vector with variables from fitting

    Parameters
    ----------
    x : [float]
        unscaled variable vector in the order C, b_0, Q
        Langmuir isotherms are calculated for this parameterset of the adsorbent
    input_isotherms : [input_isotherm]
        array of input_isotherm objects with all information of experimental or simulated isotherms
        temperatures are taken from this array to calculate Langmuir isotherms

    Returns
    -------
    langmuir_isotherms : [langmuir_isotherm]
        array of langmuir_isotherm objects with all informations of calculated Langmuir isotherms
    """

    langmuir_isotherms = []
    for input_isotherm_i in input_isotherms:

        T = input_isotherm_i.temperature
        C = x[0] * MOL/KILOGRAM
        b_0 = x[1] * 1/BAR
        Q = x[2] * JOULE/MOL
        
        pressures = input_isotherm_i.pressures

        langmuir_isotherm_i = langmuir_isotherm(C, b_0, Q)
        langmuir_isotherm_i.add_isotherm_values(pressures, T)
        langmuir_isotherms.append(langmuir_isotherm_i)
        
                
    return langmuir_isotherms