# ©2024 ETH Zurich, Fabian Mayer; D-MAVT; Energy and Process Systems Engineering

from feos.si import *

from toth_model import toth_isotherm


def calc_toth_isotherms(x, input_isotherms):
    """
    calculates toth isotherms based on fluid and temperature in input isotherms and unscaled x vector with variables from fitting

    Parameters
    ----------
    x : [float]
        unscaled variable vector in the order C, b_0, Q, n_0, alpha
        Toth isotherms are calculated for this parameterset of the adsorbent
    input_isotherms : [input_isotherm]
        array of input_isotherm objects with all information of experimental or simulated isotherms
        temperatures are taken from this array to calculate Toth isotherms

    Returns
    -------
    toth_isotherms : [toth_isotherm]
        array of toth_isotherm objects with all informations of calculated Toth isotherms
    """

    toth_isotherms = []
    for input_isotherm_i in input_isotherms:

        T = input_isotherm_i.temperature
        C = x[0] * MOL/KILOGRAM
        b_0 = x[1] * 1/BAR
        Q = x[2] * JOULE/MOL
        n_0 = x[3]
        #alpha = x[4]
        alpha = 0
        
        pressures = input_isotherm_i.pressures

        toth_isotherm_i = toth_isotherm(C, b_0, Q, n_0, alpha)
        toth_isotherm_i.add_isotherm_values(pressures, T)
        toth_isotherms.append(toth_isotherm_i)
        
                
    return toth_isotherms