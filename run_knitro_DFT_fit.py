# ©2024 ETH Zurich, Fabian Mayer; D-MAVT; Energy and Process Systems Engineering

from knitro import *
import numpy as np

from feos.si import *

from calc_DFT_isotherm import calc_DFT_isotherms
from error_function import error_function
from read_optimization_input import read_initial_values, scale_var


def knitro_DFT_fit(input_isotherms, error_metric, geometry):

    # user inputs
    path_bounds = "optimization_input/bounds_DFT.toml"
    path_initial_values = "optimization_input/initial_values_DFT.toml"
    # end user inputs


    # CALLBACK FUNCTION
    # objective function and constraints
    def callbackEvalF(kc, cb, evalRequest, evalResult, userParams):
        """
        Callback function is called in each iteration to evaluate the black-box process.
        """

        if evalRequest.type != KN_RC_EVALFC:
            print ("*** callbackEvalF incorrectly called with eval type %d" % evalRequest.type)
            return -1
        x = evalRequest.x
        print("Variables scaled:   ", x)

        try:
            x_unscaled = scale_var(x, path_bounds, unscale=True)
            print("Variables unscaled: ", x_unscaled)

            DFT_isotherms = calc_DFT_isotherms(x_unscaled, input_isotherms, geometry)

            obj_fct_value, _ = error_function(input_isotherms, DFT_isotherms, error_metric=error_metric)

            if np.isnan(obj_fct_value):
                raise Exception("Objective function value is NaN, set to high value.")

        except:
            # set objective function value to high value in case scaling or isotherm calculation was not successfull
            obj_fct_value = 1e4


        print("Objective function value: " + str(obj_fct_value))

        # pass objective function value to knitro
        evalResult.obj = obj_fct_value

        return 0



    # Create a new Knitro solver instance.
    try:
        kc = KN_new()
    except:
        print("Failed to find a valid license.")
        quit()


    # Read knitro.opt file to set options.
    KN_load_param_file(kc, "optimization_input/knitro_nlp.opt")

    # Add variables
    n = 4
    KN_add_vars(kc, n)

    # Set upper and lower bounds to 0 and 1
    xLoBnds=[0.0 for i in range(n)]
    xUpBnds=[1.0 for i in range(n)]
    KN_set_var_lobnds(kc, xLoBnds=xLoBnds)
    KN_set_var_upbnds(kc, xUpBnds=xUpBnds)


    # Set initial values 
    x_init_unscaled = read_initial_values(path_initial_values)
    x_init = scale_var(x_init_unscaled, path_bounds)
        
    KN_set_var_primal_init_values(kc, xInitVals=x_init)


    # Create callback
    #cb = KN_add_eval_callback(kc, evalObj = True, indexCons=iEvalCon, funcCallback = callbackEvalF)
    cb = KN_add_eval_callback(kc, evalObj = True, funcCallback = callbackEvalF)


    # Set direction of optimization.
    KN_set_obj_goal(kc, KN_OBJGOAL_MINIMIZE)

    # Solve the problem.
    nStatus = KN_solve (kc)

    # Obtaining solution information.
    nStatus, objSol, x, lambda_ = KN_get_solution (kc)
    tcpu = KN_get_solve_time_cpu(kc)
    treal = KN_get_solve_time_real(kc)

    # Objective value evaluation
    print("Optimal objective value  = %e" % objSol)
    print("Total CPU time           = %f" % tcpu)
    print("Total real time          = %f" % treal)
    print("Optimal x")
    for i in range (n):
        print ("  x[%d] = %e " % (i, x[i]))
    print("  KKT optimality violation = %e" % KN_get_abs_opt_error (kc))
    print("  Feasibility violation    = %e" % KN_get_abs_feas_error (kc))

    # Delete the Knitro solver instance.
    KN_free (kc)

    # check if results are close to bounds of variables
    closetobounds_total = 0
    closetobounds = [False for i in range(n)]
    for i, x_i in enumerate(x):
        if x_i < 0.05 or x_i > 0.95:
            closetobounds[i] = True
            closetobounds_total = 1

    x_unscaled = scale_var(x, path_bounds, unscale=True)

    return x_unscaled, objSol, closetobounds_total, closetobounds


