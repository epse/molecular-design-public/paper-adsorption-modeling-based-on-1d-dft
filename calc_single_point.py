# ©2024 ETH Zurich, Fabian Mayer; D-MAVT; Energy and Process Systems Engineering

from feos.dft import PhaseEquilibrium, DFTSolver, State
from feos.si import *

import numpy as np



def calc_loading_single_point(T, p, func, pore, expected_type = 1, debug=0):
    """
    calculates total loading, internal energy and enthalpy for given temperature and pressure (single point of an isotherm)
    
    Parameters
    ----------
    T : SI
        temperature
    p : SI
        pressure
    func : functional
        functional of fluid
    pore : pore
        pore object
    expected_type : int, opt
        specifies expected type of isotherm according to IUPAC classification, only 1 (default) or 5 tested as of right now
    debug : int, opt
        debug mode with more returns for problem analyses

    Returns
    -------
    total_moles : SI
        loading in mol/pore
    U : SI
        internal energy per pore
    h : SI
        enthalpy of bulk phase
    """
    
    if expected_type == 1:

        try:
            profile = standard_initialization(func, T, p, pore)
            total_moles = profile.total_moles
            U = profile.internal_energy()
            h = profile.bulk.molar_enthalpy()
            return_code = 0
        
        except:
            # match to write NaN with correct units (according to geometry) to total_moles
            match pore.geometry:
                case pore.geometry.Cartesian.Spherical:
                    total_moles = np.nan*MOL
                case pore.geometry.Cartesian.Cylindrical:
                    total_moles = np.nan*MOL/(METER)
                case pore.geometry.Cartesian.Cartesian:
                    total_moles = np.nan*MOL/(METER**2)
            U = np.nan*JOULE
            h = np.nan*JOULE/MOL
            profile = np.nan
            return_code = -1

        grand_canonical_potential_delta = 0

    elif expected_type == 5:

        vle = PhaseEquilibrium.pure(func, T)

        try:
            profile_empty = standard_initialization(func, T, p, pore)
            grand_canonical_potential_empty = profile_empty.grand_potential/JOULE
        except:
            profile_empty = np.nan
            grand_canonical_potential_empty = 0

        try:   
            profile_full = vle_initialization(vle, func, T, p, pore)
            grand_canonical_potential_full = profile_full.grand_potential/JOULE
        except:
            profile_full = np.nan
            grand_canonical_potential_full = 0


        if grand_canonical_potential_empty < 0 or grand_canonical_potential_full < 0:
            if grand_canonical_potential_empty < grand_canonical_potential_full:
                # empty pore stabler

                total_moles = profile_empty.total_moles
                U = profile_empty.internal_energy()
                h = profile_empty.bulk.molar_enthalpy()
                profile = profile_empty
                return_code = 1

            else:
                # full pore stabler

                total_moles = profile_full.total_moles
                U = profile_full.internal_energy()
                h = profile_full.bulk.molar_enthalpy()
                profile = profile_full
                return_code = 2        

        else: 
            #calculation failed, return np.nan [with correct units (according to geometry)]
            match pore.geometry:
                case pore.geometry.Cartesian.Spherical:
                    total_moles = np.nan*MOL
                case pore.geometry.Cartesian.Cylindrical:
                    total_moles = np.nan*MOL/(METER)
                case pore.geometry.Cartesian.Cartesian:
                    total_moles = np.nan*MOL/(METER**2)
            total_moles = np.nan*MOL
            U = np.nan*JOULE
            h = np.nan*JOULE/MOL
            profile = np.nan
            return_code = 5

        grand_canonical_potential_delta = grand_canonical_potential_empty - grand_canonical_potential_full

    else:
        raise ValueError('Your specified expected isotherm type is not implemented. Please choose a different type. Currently available types: 1, 5')
    

    if debug == 1:
        #return total_moles, U, h, return_code, grand_canonical_potential_empty, grand_canonical_potential_full, grand_canonical_potential_delta, profile_full, profile_empty
        return total_moles, U, h, return_code, profile
    else:
        return total_moles, U, h



def standard_initialization(func, T, p, pore):
    
    try:
        profile = pore.initialize(State(func, T, pressure=p)).solve()
    except:
        solver = DFTSolver().picard_iteration(max_iter=100, tol=1e-10, damping_coefficient=0.05).anderson_mixing()
        profile = pore.initialize(State(func, T, pressure=p)).solve(solver=solver)
        #profile = pore.initialize(State(func, T, pressure=p)).solve(solver=solver, debug=True)
    return profile


def vle_initialization(vle, func, T, p, pore):
    try:
        profile_vle = pore.initialize(vle.liquid).solve()
        profile = pore.initialize(State(func, T, pressure=p), profile_vle.density).solve()
    except:
        solver = DFTSolver().picard_iteration(max_iter=100, tol=1e-10, damping_coefficient=0.05).anderson_mixing()
        profile_vle = pore.initialize(vle.liquid).solve(solver=solver)
        profile = pore.initialize(State(func, T, pressure=p), profile_vle.density).solve(solver=solver)
    return profile

