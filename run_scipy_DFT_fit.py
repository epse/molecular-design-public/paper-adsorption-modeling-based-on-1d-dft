# ©2024 ETH Zurich, Fabian Mayer; D-MAVT; Energy and Process Systems Engineering

from scipy.optimize import minimize
import numpy as np

from feos.si import *

from calc_DFT_isotherm import calc_DFT_isotherms
from error_function import error_function
from read_optimization_input import read_initial_values, scale_var


def scipy_DFT_fit(input_isotherms, error_metric, geometry):

    # user inputs
    path_bounds = "optimization_input/bounds_DFT.toml"
    path_initial_values = "optimization_input/initial_values_DFT.toml"
    # end user inputs

    # CALLBACK FUNCTION
    # objective function and constraints
    def callbackEvalF(x):
        """
        Callback function is called in each iteration to evaluate the black-box process.
        """

        print("Variables scaled:   ", x)

        try:
            x_unscaled = scale_var(x, path_bounds, unscale=True)
            print("Variables unscaled: ", x_unscaled)


            DFT_isotherms = calc_DFT_isotherms(x_unscaled, input_isotherms, geometry)

            obj_fct_value, _ = error_function(input_isotherms, DFT_isotherms, error_metric=error_metric)

            if np.isnan(obj_fct_value):
                raise Exception("Objective function value is NaN, set to high value.")

        except:
            # set objective function value to high value in case scaling or isotherm calculation was not successfull
            obj_fct_value = 1e4


        print("Objective function value: " + str(obj_fct_value))

        return obj_fct_value



    # Add variables
    n = 4

    # Set upper and lower bounds to 0 and 1
    xLoBnds=[0.0 for i in range(n)]
    xUpBnds=[1.0 for i in range(n)]
    x_bounds = list(zip(xLoBnds, xUpBnds))


    # Set initial values 
    x_init_unscaled = read_initial_values(path_initial_values)
    x_init = scale_var(x_init_unscaled, path_bounds)
        

    # Create callback
    result = minimize(callbackEvalF, x_init, bounds = x_bounds)


    x_unscaled = scale_var(result.x, path_bounds, unscale=True)

    return x_unscaled, result.fun





