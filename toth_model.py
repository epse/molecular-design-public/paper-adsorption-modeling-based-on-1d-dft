# ©2024 ETH Zurich, Fabian Mayer; D-MAVT; Energy and Process Systems Engineering

from feos.si import *
import numpy as np

class toth_isotherm:
    """
    A class used to calculate an isotherm using the temperature-dependent Toth model
    
    Attributes
    ----------
    
    Methods
    -------
    
    """
    def __init__(self, C, b_0, Q, n_0, alpha):
        """
        Initialize Toth isotherm object
        
        Parameters
        ----------
        C : float
            parameter C of Toth model (maximum loading, usually in mol/kg)
        b_0 : float
            parameter b_0 of Toth model (describes the slope of the isotherm, usually in 1/bar)
        Q : float
            parameter Q of Toth model (describes the temperature-dependence of the isotherm, usually in J/mol)
        n_0 : float
            parameter n_0 of Toth model (describes the  surface heterogeneity)
        alpha : float
            parameter alpha of Toth model (describes the temperature-dependence of the surface heterogeneity n)
        """
        self.C = C
        self.b_0 = b_0
        self.Q = Q
        self.n_0 = n_0
        self.alpha = alpha
    
    def calc_loading(self):
        """
        Calculate loadings of Toth isotherm object
        
        Parameters
        ----------
        
        Returns
        -------
        loadings : [SI]
            loadings of Toth isotherm object
        """

        T_0 = 298.15*KELVIN

        b = self.b_0 * np.exp(self.Q / (RGAS * self.temperature))

        n = self.n_0 + self.alpha * (1 - T_0/self.temperature)
        
        loadings = self.C * (b * self.pressures) / (1 + (b * self.pressures) ** n) ** (1 / n)
        
        return loadings
    
    def add_isotherm_values(self, pressures, temperature):
        """
        Add pressures, temperature, and loadings to Toth isotherm object
        
        Parameters
        ----------
        pressures : [SI]
            pressure vector for isotherm calculation
        temperature : SI
            Temperature of isotherm
        """
        self.temperature = temperature
        self.pressures = pressures
        self.loadings = self.calc_loading()
