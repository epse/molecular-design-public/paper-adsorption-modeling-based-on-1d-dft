#%% 
# ©2024 ETH Zurich, Fabian Mayer; D-MAVT; Energy and Process Systems Engineering

# This script plots the share of datapoints below a certain error over the error metric for fitted 1D-DFT parameters and extrapolated 1D-DFT parameters from a fit to another fluid

#%% Import packages
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np


#%% Input


# paths to the csv files containing the error data
path_input_file_fit = "results/xxx.csv"
path_input_file_extrapolation = "results/xxx.csv"

# name of error metric for which the plot should be created
error_metric = 'MARD'
#error_metric = 'MAD'

extrapolation_method = "fluidextrapolation"  # currently only fluidextrapolation implemented


# specify the molecules for which the plot should be created
molecules = ['N2', 'CH4']
#molecules = ['CO2', 'N2']


figsize = (3.2, 2.5)
fontsize = 11

# latex labels for molecules
molecule_labels = {'CO2': r'$\mathrm{CO}_2$', 'N2': r'$\mathrm{N}_2$', 'CH4': r'$\mathrm{CH}_4$'}

# specify the colors for the different molecules
colors_molecules = {'CO2': 'green', 'N2': 'blue', 'CH4': 'red'}

# specify linestyle for the different molecules
#linestyles_molecules = {'CO2': '--', 'N2': '-', 'CH4': '--'}
linestyles_molecules_extrapolation = {'CO2': '-', 'N2': '-', 'CH4': '-'}
linestyles_molecules_fit = {'CO2': '--', 'N2': '--', 'CH4': '--'}

# set x-limit
XLIM = 3

# create array for markers of specific adsorbent
error_metric_marker = [5.84875306593903, 3.16487732036592, 3.17031370947647, 0.899926599781073]  #RSM2682: [CH4 extrapolated, N2 extrapolated, CH4 fit, N2 fit]


#%% End Input

#%% Input data processing


# read error data from csv files
error_data_extrapolation = pd.read_csv(path_input_file_extrapolation)
error_data_fit = pd.read_csv(path_input_file_fit)


# extract column with errors for each molecule
error_data_molecules_extrapolation = {}
error_data_molecules_fit = {}
for molecule in molecules:
    error_data_molecules_extrapolation[molecule] = []
    error_data_molecules_fit[molecule] = []
    for i in range(len(error_data_extrapolation)):
        if error_data_extrapolation['Molecule'][i] == molecule and np.isnan(error_data_extrapolation[error_metric][i]) == False:
            error_data_molecules_extrapolation[molecule].append(error_data_extrapolation[error_metric][i])
    for i in range(len(error_data_fit)):
        if error_data_fit['Molecule'][i] == molecule and np.isnan(error_data_fit[error_metric][i]) == False:
            error_data_molecules_fit[molecule].append(error_data_fit[error_metric][i])

# sort lists of error data
for molecule in molecules:
    error_data_molecules_extrapolation[molecule] = sorted(error_data_molecules_extrapolation[molecule])
    error_data_molecules_fit[molecule] = sorted(error_data_molecules_fit[molecule])
    
    

# calculate share of datapoints below a certain error
share_below_error_extrapolation = {}
share_below_error_fit = {}
for molecule in molecules:
    share_below_error_extrapolation[molecule] = []
    for i in range(len(error_data_molecules_extrapolation[molecule])):
        share_below_error_extrapolation[molecule].append((i+1)/len(error_data_molecules_extrapolation[molecule])*100)

    share_below_error_fit[molecule] = []
    for i in range(len(error_data_molecules_fit[molecule])):
        share_below_error_fit[molecule].append((i+1)/len(error_data_molecules_fit[molecule])*100)



# get share at which the marker is located
share_below_error_marker = np.zeros(len(error_metric_marker))

for i, error_i in enumerate(error_data_molecules_extrapolation['CH4']):
    if np.isclose(error_i, error_metric_marker[0]):
        share_below_error_marker[0] = share_below_error_extrapolation['CH4'][i]
        break
for i, error_i in enumerate(error_data_molecules_extrapolation['N2']):
    if np.isclose(error_i, error_metric_marker[1]):
        share_below_error_marker[1] = share_below_error_extrapolation['N2'][i]
        break
for i, error_i in enumerate(error_data_molecules_fit['CH4']):
    if np.isclose(error_i, error_metric_marker[2]):
        share_below_error_marker[2] = share_below_error_fit['CH4'][i]
        break
for i, error_i in enumerate(error_data_molecules_fit['N2']):
    if np.isclose(error_i, error_metric_marker[3]):
        share_below_error_marker[3] = share_below_error_fit['N2'][i]
        break




#%% Plotting


# set fonts for plots to latex fonts
plt.rcParams.update({"text.usetex": True, "pgf.texsystem": "pdflatex", 'font.family': 'sans-serif', 'font.sans-serif': 'Computer Modern Sans Serif', 'font.size': fontsize})


# create figure
fig, ax = plt.subplots(figsize=figsize)

# plot share of datapoints below a certain error over the error metric
for molecule in molecules:
    ax.plot(error_data_molecules_fit[molecule], share_below_error_fit[molecule], label=f'{molecule_labels[molecule]} fit', color=colors_molecules[molecule], linestyle=linestyles_molecules_fit[molecule])
    ax.plot(error_data_molecules_extrapolation[molecule], share_below_error_extrapolation[molecule], label=f'{molecule_labels[molecule]} from {molecule_labels[molecules[(molecules.index(molecule)+1)%len(molecules)]]}', color=colors_molecules[molecule], linestyle=linestyles_molecules_extrapolation[molecule])

# set x- and y-labels
ax.set_xlabel(f'{error_metric} in \%' if error_metric == 'MARD' else error_metric, fontsize=fontsize)
ax.set_ylabel('share of adsorbents in \%', fontsize=fontsize)

# set x- and y-ticks
ax.tick_params(axis='both', which='major', labelsize=fontsize)
ax.tick_params(axis='both', which='minor', labelsize=fontsize)

# set x- and y-limits
if error_metric == 'MARD':
    ax.set_xlim(left=0, right = 100)
else:
    ax.set_xlim(left=0, right = XLIM)    
ax.set_ylim(bottom=0, top=100)

# set steps of x-ticks
ax.xaxis.set_major_locator(plt.MultipleLocator(10))

# set steps of y-ticks
#ax.yaxis.set_major_locator(plt.MultipleLocator(10))

# set legend
ax.legend(fontsize=fontsize)

# locate legend in lower right corner
ax.legend(loc='lower right', fontsize=fontsize)

# plot markers for specific adsorbent
ax.plot(error_metric_marker, share_below_error_marker, marker='d', markersize=3, linestyle='None', color='black')

# save figure
fig.savefig(f'{path_input_file_extrapolation[0:-4]}_share_over_{error_metric}.pdf', bbox_inches='tight')

#%% End Plotting



