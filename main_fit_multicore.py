#%% 
# ©2024 ETH Zurich, Fabian Mayer; D-MAVT; Energy and Process Systems Engineering


# This script fits the isotherms in the input file with the selected model (langmuir, toth, sips, or 1D-DFT) in parallel using multiple cores.

#%% Import packages
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from multiprocessing import Pool, Manager

from feos.si import *
from input_processing import prisma_4temp_csv_to_input_isotherm
from run_knitro_DFT_fit import knitro_DFT_fit
from run_scipy_DFT_fit import scipy_DFT_fit
from run_knitro_empirical_isotherm_model_fit import knitro_empirical_model_fit
from run_scipy_empirical_isotherm_model_fit import scipy_empirical_model_fit

#%% Input


path_input_file = "input_data/xxx.csv"
path_results_file = "results/xxx.csv"
n_processes = 10  # number of processes to use

model_to_fit = "DFT"  # langmuir, toth, sips, or DFT
geometry = ["Spherical", "Cylindrical", "Cartesian"] # geometry of the pore in the DFT calculation. If more than one geometry is given, the geometry with the smallest fitting error is used for the results. ["Spherical", "Cylindrical", or "Cartesian"]


# number of isotherms per batch. batch means a group of isotherms of the same adsorbent and the fluids that should be considered in the fit.
# E.g. if one fluid should be considered in the fit, one batch contains isotherms at different temperatures for the same adsorbent and the same fluid
#      if two fluids should be considered in the fit, one batch contains isotherms at different temperatures for the same adsorbent and the two fluids
n_isotherms_per_batch = 10

isotherms_to_fit = [0, 5] # list of isotherms to fit simultaneously per batch. E.g. [0, 1] means that the first and second isotherm of each batch will be fitted simultaneously while the remaining isotherms of the batch are not considered in the fit

# specify name of error metric that should be used as objective function in the fit. See file error_function.py for available error metrics.
error_metric = 'SSE'

#%% End Input

input_isotherms, input_data = prisma_4temp_csv_to_input_isotherm(path_input_file, column_name_loadings='Uptake [mol/kg]')

# append input dataframe with columns for results
input_data[f'{model_to_fit}_parameters_fit'] = [np.array([]) for i in range(len(input_data))]
input_data['fitting_error'] = [None] * len(input_data)
input_data['geometry'] = [None] * len(input_data)
input_data['closetobounds_total'] = [None] * len(input_data)
input_data['closetobounds'] = [None] * len(input_data)


# create list of batches over which will be iterated
iterator = list(range(0, np.size(input_isotherms)//n_isotherms_per_batch))


# Define the function to fit each isotherm
def fit_isotherm(args):
    i = args
    input_isotherms_batch = [input_isotherms[i*n_isotherms_per_batch + j] for j in isotherms_to_fit]
    match model_to_fit:
        case model if model in ["langmuir", "toth", "sips"]:
            #x, obj_fct_value_solution, closetobounds_total, closetobounds = knitro_empirical_model_fit(input_isotherms_batch, error_metric, model_to_fit)
            x, obj_fct_value_solution, closetobounds_total, closetobounds = scipy_empirical_model_fit(input_isotherms_batch, error_metric, model_to_fit)
            geometry_min = None
        case "DFT":
            x_min = None
            obj_fct_value_solution_min = np.inf
            geometry_min = None
            for geometry_i in geometry:
                #x, obj_fct_value_solution, closetobounds_total, closetobounds = knitro_DFT_fit(input_isotherms_batch, error_metric, geometry_i)
                x, obj_fct_value_solution, closetobounds_total, closetobounds = scipy_DFT_fit(input_isotherms_batch, error_metric, geometry_i)
                if (not np.isnan(obj_fct_value_solution)) and (obj_fct_value_solution < obj_fct_value_solution_min):
                    x_min = x
                    obj_fct_value_solution_min = obj_fct_value_solution
                    geometry_min = geometry_i
            x = x_min
            obj_fct_value_solution = obj_fct_value_solution_min


    print(f'batch {i} DONE')

    return i, x, obj_fct_value_solution, geometry_min, closetobounds_total, closetobounds

# Run fitting in parallel
with Pool(processes=n_processes) as pool:
    # Map the fit_isotherm function over the iterator
    results = pool.map(fit_isotherm, iterator)
    


#%% Collect results
for i, x, obj_fct_value_solution, geometry_min, closetobounds_total, closetobounds in results:
    for j in isotherms_to_fit:
        input_data.at[i*n_isotherms_per_batch + j, f'{model_to_fit}_parameters_fit'] = np.array(x)
        input_data.at[i*n_isotherms_per_batch + j, 'fitting_error'] = obj_fct_value_solution
        input_data.at[i*n_isotherms_per_batch + j, 'geometry'] = geometry_min
        input_data.at[i*n_isotherms_per_batch + j, 'closetobounds_total'] = closetobounds_total
        input_data.at[i*n_isotherms_per_batch + j, 'closetobounds'] = closetobounds
    

# Save results to a file
input_data.to_csv(path_results_file, index=False)
