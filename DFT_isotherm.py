# ©2024 ETH Zurich, Fabian Mayer; D-MAVT; Energy and Process Systems Engineering

from feos.si import *
from feos.dft import ExternalPotential, Pore1D, PhaseEquilibrium, HelmholtzEnergyFunctional, Adsorption1D, DFTSolver, State, Geometry
from feos.pcsaft import PcSaftParameters
from feos.eos import EquationOfState

import numpy as np

from calc_single_point import calc_loading_single_point

class DFT_isotherm:
    """
    A class used to calculate an isotherm using 1D-DFT
    
    Attributes
    ----------

    Methods
    -------

    """

    def __init__(self, adsorbate, path_pcsaft_parameters, T, sigma, epsilon, rho, pore_size, geometry, internal_surface, pressures, pressure_option = "from input", pressures_length = 0, expected_type = 1, excess = False, single_point = False):
        """
        Parameters
        ----------
        adsorbate : str
            The name of the adsorbate
        path_pcsaft_parameters : str
            Path to json-file with pc saft parameters of fluid
        T : SI
            Temperature of isotherm
        sigma : float
            sigma parameter of adsorbent
        epsilon : float
            epsilon parameter of adsorbent
        rho : float
            rho parameter of adsorbent
        pore_size : SI
            size of pore (radius for spherical)
        geometry : str
            geometry of pore according to feos
        internal_surface : SI
            internal surface area of adsorbent per mass unit of adsorbent
        pressures : [SI]
            pressure vector for isotherm calculation, minimum and maximum pressure for pressure options "lin" and "log"
        pressure_option : str, opt
            determine which pressures should be used for isotherm calculation:
            "from input": use exact same pressures from parameter pressures (default)
            "lin": distribute pressures linearly between first and last element of parameter pressures
            "log": distribute pressures logarithmically between first and last element of parameter pressures
            "vle_lin": distribute pressures linearly accordingly to vle
            "vle_log": distribute pressures logarithmically accordingly to vle
        pressures_length : int, opt
            length of pressure vector if pressure_option is not "from input"
        expected_type : int, opt
            specifies expected type of isotherm according to IUPAC classification, only 1 (default) or 5 tested as of right now
        excess : boolean, opt
            specify if loading values are excess loading (True) or absolute loading (False, default)
        single_point : boolean, opt
            decision variable if single point method should be used for all pressures or not
        """




        self.adsorbate = adsorbate
        self.temperature = T
        self.sigma = sigma
        self.epsilon = epsilon
        self.rho = rho
        self.pore_size = pore_size
        self.internal_surface = internal_surface
        self.type_IUPAC = expected_type
        self.excess = excess

        self.potential = ExternalPotential.LJ93(sigma, epsilon, rho)

        self.pore = Pore1D(eval("Geometry."+geometry), pore_size, self.potential, n_grid = 1024)  #TODO: read n_grid from file with all constants
        #self.pore = Pore1D(eval("Geometry."+geometry), pore_size, self.potential, n_grid = 128)  #TODO: read n_grid from file with all constants

        self.saft_parameters = PcSaftParameters.from_json([adsorbate], path_pcsaft_parameters) #TODO: read path from file with all constants
        self.func = HelmholtzEnergyFunctional.pcsaft(self.saft_parameters)
        self.eos = EquationOfState.pcsaft(self.saft_parameters)

        #set initial pressures for isotherm calculation according to pressure_option
        match pressure_option:
            case "vle_log":
                # if pressures should be set according to vle, calculate vle
                vle = PhaseEquilibrium.pure(self.func, T)
                self.pressures_init = SIArray1.logspace(0.001*vle.liquid.pressure(), 1.1*vle.liquid.pressure(), pressures_length) #TODO: test!!!

            case "vle_lin":
                # if pressures should be set according to vle, calculate vle
                vle = PhaseEquilibrium.pure(self.func, T)
                self.pressures_init = SIArray1.linspace(0.001*vle.liquid.pressure(), 1.1*vle.liquid.pressure(), pressures_length)

            case "log":
                self.pressures_init = SIArray1.logspace(pressures[0], pressures[-1], pressures_length)

            case "lin":
                self.pressures_init = SIArray1.linspace(pressures[0], pressures[-1], pressures_length)

            case _:
                self.pressures_init = pressures
        
        # set dft_pressures to pressures_init to be able to use dft_pressures if single_point==True; if single_point==False, dft_pressures will be overwritten
        dft_pressures = self.pressures_init
        # create dft_loadings in correct unit (according to geometry):
        match geometry: 
            case 'Spherical':
                dft_loadings = SIArray1(np.zeros(np.size(dft_pressures/PASCAL))*MOL)
            case 'Cylindrical':
                dft_loadings = SIArray1(np.zeros(np.size(dft_pressures/PASCAL))*MOL/METER)
            case 'Cartesian':
                dft_loadings = SIArray1(np.zeros(np.size(dft_pressures/PASCAL))*MOL/(METER**2))

        if not single_point:
            if expected_type == 1:

                try:
                    self.dft_isotherm = Adsorption1D.adsorption_isotherm(self.func, T, self.pressures_init, self.pore)

                    #raise error if at least one value is nan to try again with better solver options
                    if np.isnan(np.sum(self.dft_isotherm.pressure/PASCAL)):
                        raise

                except:
                    try:
                        solver = DFTSolver().picard_iteration(max_iter=100, tol=1e-10, damping_coefficient=0.05).anderson_mixing()
                        self.dft_isotherm = Adsorption1D.adsorption_isotherm(self.func, T, self.pressures_init, self.pore, solver=solver)
                    except:
                        #calculation of isotherms not successfull, calculate loadings with single point method for all pressures
                        single_point = True


            elif expected_type == 5:

                try:
                    self.dft_isotherm = Adsorption1D.equilibrium_isotherm(self.func, T, self.pressures_init, self.pore)

                    #raise error if at least one value is nan to try again with better solver options
                    if np.isnan(np.sum(self.dft_isotherm.pressure/PASCAL)):
                        raise

                except:
                    try:
                        solver = DFTSolver().picard_iteration(max_iter=100, tol=1e-10, damping_coefficient=0.05).anderson_mixing()
                        self.dft_isotherm = Adsorption1D.equilibrium_isotherm(self.func, T, self.pressures_init, self.pore, solver=solver)
                    except:
                        #calculation of isotherms not successfull, calculate loadings with single point method for all pressures
                        single_point = True

            else:
                raise ValueError('Your specified expected isotherm type is not implemented. Please choose a different type. Currently available types: 1, 5')


        # check again if single_point == True because it could change to True if DFT did not converge with all solver options
        if not single_point:

            dft_pressures = self.dft_isotherm.pressure
            dft_loadings = self.dft_isotherm.total_adsorption

            #if at least one value of isotherm is NaN and pore filling equilibrium pressure was added to pressure vector, use single point method to recalculate all loadings to avoid confusion with indices of pressure vector
            if np.isnan(np.sum(dft_pressures/PASCAL)) and np.size(self.pressures_init/PASCAL) == np.size(dft_pressures/PASCAL):
                single_point = True
                # reset pressure vector to initial values and loadings vector to empty vector
                dft_pressures = self.pressures_init
                # create dft_loadings in correct unit (according to geometry):
                match geometry: 
                    case 'Spherical':
                        dft_loadings = SIArray1(np.zeros(np.size(dft_pressures/PASCAL))*MOL)
                    case 'Cylindrical':
                        dft_loadings = SIArray1(np.zeros(np.size(dft_pressures/PASCAL))*MOL/METER)
                    case 'Cartesian':
                        dft_loadings = SIArray1(np.zeros(np.size(dft_pressures/PASCAL))*MOL/(METER**2))

        # use single point method if any value is NaN, isotherm calculation did not converge, or if single point method should be used for all pressures
        for i,p in enumerate(dft_pressures):
            if single_point or np.isnan(p/PASCAL):
                w_per_pore, _, _ = calc_loading_single_point(T, self.pressures_init[i], self.func, self.pore, expected_type)
                dft_loadings[i] = w_per_pore


        # transfer absolute loading in moles/pore to absolute or excess loading in mol/kg
        loadings = []
        
        for i,w_per_pore in enumerate(dft_loadings):
            
            if excess:
                #calculate excess loading in mol/pore
                bulk_state = State(eos=self.eos, temperature=T, pressure = dft_pressures[i])
                w_per_pore = w_per_pore - bulk_state.density*self.pore.pore_volume

            #calculate loading in mol/kg
            match geometry:
                case 'Spherical':
                    loading = w_per_pore/(4*np.pi*(pore_size)**2) * internal_surface
                case 'Cylindrical':
                    loading = w_per_pore*1*METER/(2*np.pi*pore_size*1*METER) * internal_surface
                case 'Cartesian':
                    loading = w_per_pore * internal_surface

            loadings.append(loading)
        


        #do not remove negative values in this case because then element-wise calculation of objective function for fitting would not work
        ##remove negative values from loading array
        #index_delete = []
        #for i,w in enumerate(loadings):
        #    if w/MOL*KILOGRAM <= -1e-2:
        #        index_delete.append(i)
        #loadings_clean = [i for j, i in enumerate(loadings) if j not in index_delete]
        #dft_pressures_clean = [i for j, i in enumerate(dft_pressures) if j not in index_delete]
        loadings_clean = loadings
        dft_pressures_clean = dft_pressures

        #calculate loading in kg/kg
        loadings_kg = []
        for i,w in enumerate(loadings_clean):
            w_kg = w*self.saft_parameters.pure_records[0].molarweight*GRAM/MOL
            loadings_kg.append(w_kg)
        


        self.loadings = SIArray1(loadings_clean)
        self.loadings_kg = loadings_kg
        self.pressures = SIArray1(dft_pressures_clean)