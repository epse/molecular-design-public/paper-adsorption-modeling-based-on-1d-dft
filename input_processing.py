# ©2024 ETH Zurich, Fabian Mayer; D-MAVT; Energy and Process Systems Engineering

import pandas as pd
import numpy as np

from feos.si import *

from input_isotherm import input_isotherm


def prisma_4temp_csv_to_input_isotherm(path, column_name_loadings = 'uptake'):
    """
    reads input file and creates isotherm object for each line of input file

    Parameters
    ----------
    path : string
        path of input file
    column_name : string, opt
        name of column that contains loadings

    Returns
    -------
    input_isotherms : [input_isotherm]
        array of input_isotherm objects with all informations of isotherms from input file
    input_data : dataframe
        dataframe with raw data from input file
    """

    input_data = pd.read_csv(path)

    input_isotherms = []

    for i, p in enumerate(input_data['Pressure [bar]']):
        #if i>15:
        #    break
        #print(i)
        
        pressures = np.array(np.matrix(p)).ravel() * BAR


        try:
            #uptakes = np.array(np.matrix(input_data[column_name_loadings][i])).ravel() * MOL/(KILO*GRAM)   #does not work if loading vector contains NaN

            #replace nan with -1000, convert to float, then replace -1000 with nan
            uptakes_str = input_data[column_name_loadings][i]
            uptakes_str = uptakes_str.replace('nan','-1000')
            uptakes_float = np.array(np.matrix(uptakes_str)).ravel()
            uptakes = np.where(uptakes_float == -1000, np.nan, uptakes_float) * MOL/(KILO*GRAM)

        except:
            #in case nan management does not work, set all values of loading vector to nan
            uptakes = np.array([np.nan]*np.shape(pressures/BAR)[0])*(MOL/(KILOGRAM))

        
        T = input_data['T_ref [K]'][i] * KELVIN
        adsorbent = input_data.MOF[i]
        adsorbate_short = input_data.Molecule[i]

        if adsorbate_short == 'CO2':
            adsorbate = 'carbon dioxide'
            type_IUPAC = 1
        elif adsorbate_short == 'N2':
            adsorbate = 'nitrogen'
            type_IUPAC = 1
        elif adsorbate_short == 'CH4':
            adsorbate = 'methane'
            type_IUPAC = 1
        elif adsorbate_short == 'C2H6O':
            adsorbate = 'ethanol'
            type_IUPAC = 5

        isotherm_i = input_isotherm(adsorbent, adsorbate, adsorbate_short, T, pressures, uptakes, type_IUPAC)

        input_isotherms.append(isotherm_i)

    return input_isotherms, input_data