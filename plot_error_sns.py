#%% 
# ©2024 ETH Zurich, Fabian Mayer; D-MAVT; Energy and Process Systems Engineering

# This script plots violinplots or boxplots of the error metrics from one or several files.

#%% Import packages
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import numpy as np


#%% Input

# paths to the csv files containing the error data
# if more than one path is given, the error data from the different files will be plotted in a grouped violinplot
paths_input_file = ["results/xxx_DFT.csv", "results/xxx_toth.csv", "results/xxx_langmuir.csv"]

# error metrics for which violinplots should be created, for each metric a separate plot will be created and saved
error_metrics = ['RMSE', 'SSE', 'SAE', 'ARE', 'MARD', 'MRD', 'difference_error', 'MAD', 'MARD_max_loading', 'difference_max_loading', 'MAD_max_loading', 'initial_slope_error']
#error_metrics = ['SSE', 'MARD', 'MRD']
#error_metrics = ['MARD']


extrapolation_method = "tempextrapolation"

# choose the style of the plot: 'bp' for boxplot, 'vp' for violinplot
plot_style = 'bp' # 'vp' or 'bp'

# specify the name(s) of the models used to generate the data in the csv files
models = ['DFT', 'toth', 'langmuir']
#models = ['DFT']
# specify name(s) of the models that should be used in legend
models_legend = ["1D-DFT", "Toth", "Langmuir"]
#models_legend = ["1D-DFT"]

# specify the molecules for which violinplots should be created, for each molecule a separate plot will be created and saved
# (molecules can also be given as lists if data from several molecules should be plotted in the same plot)
#molecules = ['N2']
molecules = ['CO2', 'N2']
#molecules = ['CH4', 'N2']
#molecules = [['CO2', 'N2']]
#molecules = [['CO2', 'N2']]
#molecules = [['CH4', 'N2']]
#molecules = [['CO2', 'CH4']]

# specify the temperatures of isotherms in the csv files. The temperatures will be used as x-labels in the violinplots
T_in = [298.15, 323.15, 348.15, 373.15, 398.15]
#T_in = [298.15]

#figsize = (3.5, 2.5)
figsize = (3.2, 2.5)
fontsize = 11

# latex labels for molecules
molecule_labels = {'CO2': r'$\mathrm{CO}_2$', 'N2': r'$\mathrm{N}_2$', 'CH4': r'$\mathrm{CH}_4$'}

colors_model = {'1D-DFT': 'green', 'Langmuir': 'orange', 'Toth': 'blue', 'Sips': 'orange'}

#%% End Input

#%% Input data processing

# transform T_in to string
T_in_str = [str(T) for T in T_in]

# read error data from csv files
error_data = []
for path_input_file in paths_input_file:
    error_data.append(pd.read_csv(path_input_file))


#%% Function to draw and save plots
def draw_and_save_errorplot(df, molecule, error_metric):
    
    # set fonts for plots to latex fonts
    plt.rcParams.update({"text.usetex": True, "pgf.texsystem": "pdflatex", 'font.family': 'sans-serif', 'font.sans-serif': 'Computer Modern Sans Serif', 'font.size': fontsize})
    #plt.rcParams.update({"text.usetex": True, "pgf.texsystem": "pdflatex", 'font.family': 'serif', 'font.serif': 'Computer Modern', 'font.size': fontsize})
    #plt.rcParams.update({"text.usetex": True, "pgf.texsystem": "pdflatex", 'font.family': 'sans-serif', 'font.sans-serif': 'Helvetica', 'font.size': fontsize})
    
    
    


    # Create a figure instance
    fig = plt.figure(figsize=figsize)

    # Create an axes instance
    ax = fig.add_axes([0,0,1,1])

    # Create the plot
    if plot_style == 'vp':
        sns.violinplot(ax = ax,
               data = df,
               x = 'molecule_name' if extrapolation_method=='fluidextrapolation' else 'T [K]',
               y = 'error',
               hue = 'model',
               split = False,
               #bw = 0.1,
               cut = 0,
               palette=colors_model)
    
    elif plot_style == 'bp':
        sns.boxplot(ax = ax,
           data = df,
           x = 'molecule_name' if extrapolation_method=='fluidextrapolation' else 'T [K]',
           y = 'error',
           hue = 'model',
           flierprops = dict(marker='o',  markerfacecolor='none', markersize=2, markeredgecolor='black', markeredgewidth=0.5),
           palette=colors_model)

    
    else:
        raise ValueError('plot_style must be "vp" or "bp"')
        

    # draw horizontal axis line for non-absolute error metrics
    if error_metric not in ['MARD', 'MAD', 'cv', 'cv_rmse']:
        ax.axhline(y=0, color='black', linestyle='-', linewidth=0.5)
    else:
        #set lower limit of y-axis to 0 for absolute error metrics
        ax.set_ylim(bottom=0)

    # Set the labels for the x- and y-axis
    ax.set_xlabel('fluid' if extrapolation_method=='fluidextrapolation' else 'Temperature in K')
    if error_metric == 'MARD':
        ax.set_ylabel('MARD in \%')
    elif error_metric == 'MRD':
        ax.set_ylabel('MRD in \%')
    else:
        ax.set_ylabel(error_metric)

    # Set the y-axis limits
    #plt.ylim(ylim)
    #plt.ylim([0, 30])
    
    # Get the x-tick labels
    xticklabels = [item.get_text() for item in ax.get_xticklabels()]

    # Get the y-tick labels
    yticklabels = [item.get_text() for item in ax.get_yticklabels()]

    #yticklabels = np.arange(0,200,25)

    # Set the y-tick labels to strings so that the same font as for the labels is used
    yticklabels_new = []
    for tick in yticklabels:
        tick_new = ''
        for d in tick:
            if d.isdigit():
                tick_new += d
            elif d == '.':
                tick_new += d
        yticklabels_new.append(tick_new)
    ax.set_yticklabels([tick for tick in yticklabels_new])

                
                




    # Set the x-tick labels
    if extrapolation_method=='fluidextrapolation':
        ax.set_xticklabels([molecule_labels[tick] for tick in xticklabels])

    # Set the x-tick labels
    #if extrapolation_method=='tempextrapolation':
    #    ax.set_xticklabels([tick for tick in xticklabels])
    
    
    plt.legend(loc='upper left')
    #plt.legend(loc='upper left', frameon=False)
    
    if extrapolation_method == 'fluidextrapolation':
        #don't show legend for fluid extrapolation
        ax.get_legend().remove()
    
    

    

    # Save figure
    fig.savefig(f'{paths_input_file[0][0:-4]}_{error_metric}_{plot_style}_{molecule}.pdf', bbox_inches='tight')
    

    plt.close()


#%% rearange input data to dataframe and call function to draw and save violinplots, loop over error metrics and molecules

for error_metric in error_metrics:

    for molecule in molecules:
        # create dataframe with error data
        error_df = pd.DataFrame(columns=['T [K]', 'model', 'molecule_name', 'error'])
        # add error data to dataframe
        for i, path_input_file in enumerate(paths_input_file):
             for row in error_data[i].index:
                 if error_data[i].Molecule[row] in molecule:
                    if not extrapolation_method == 'fluidextrapolation':
                        error_df = pd.concat([error_df, pd.DataFrame({'T [K]': str(error_data[i].at[row, 'T_ref [K]']), 'model': models_legend[i], 'error': error_data[i].at[row, error_metric]}, index=[0])], ignore_index=True)
                    else:
                        # fluideextrapolation: usually only one model, so plot is grouped by molecule instead of model
                        error_df = pd.concat([error_df, pd.DataFrame({'T [K]': str(error_data[i].at[row, 'T_ref [K]']), 'molecule_name': error_data[i].Molecule[row], 'model': models_legend[i], 'error': error_data[i].at[row, error_metric]}, index=[0])], ignore_index=True)

             
        draw_and_save_errorplot(error_df, molecule, error_metric)







    
