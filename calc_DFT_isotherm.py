# ©2024 ETH Zurich, Fabian Mayer; D-MAVT; Energy and Process Systems Engineering

from feos.si import *

from DFT_isotherm import DFT_isotherm


def calc_DFT_isotherms(x, input_isotherms, geometry):
    """
    calculates DFT isotherms based on fluid and temperature in input isotherms and unscaled x vector with variables from fitting

    Parameters
    ----------
    x : [float]
        unscaled variable vector in the order sigma, epsilon, pore_size, internal_surface
        DFT isotherms are calculated for this description of the adsorbent
    input_isotherms : [input_isotherm]
        array of input_isotherm objects with all information of experimental or simulated isotherms
        fluids and temperatures are taken from this array to calculate DFT isotherms

    Returns
    -------
    DFT_isotherms : [DFT_isotherm]
        array of DFT_isotherm objects with all informations of calculated DFT isotherms
    """

    DFT_isotherms = []
    for input_isotherm_i in input_isotherms:

        adsorbate = input_isotherm_i.adsorbate
        T = input_isotherm_i.temperature
        sigma = x[0]
        epsilon = x[1]

        rho = 0.08 
        path_pcsaft_parameters = "input_data/gross2001.json"
        #path_pcsaft_parameters = "input_data/gross2002.json"
        #path_pcsaft_parameters = "input_data/gross2005_literature.json"

        pore_size = x[2]*ANGSTROM
        internal_surface = x[3]*METER**2/KILOGRAM
        pressures = input_isotherm_i.pressures
        excess = input_isotherm_i.excess
        type_IUPAC = input_isotherm_i.type_IUPAC

        DFT_isotherms.append(DFT_isotherm(adsorbate, path_pcsaft_parameters, T, sigma, epsilon, rho, pore_size, geometry, internal_surface, pressures, excess, expected_type=type_IUPAC))


    return DFT_isotherms