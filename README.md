# Adsorption modeling based on classical density functional theory and PC-SAFT: Temperature extrapolation and fluid transfer

In this repository, you can find the Python code for the paper "Adsorption modeling based on classical density functional theory and PC-SAFT: Temperature extrapolation and fluid transfer" by Fabian Mayer, Philipp Rehner, Jan Seiler, Johannes Schilling, Joachim Gross, and André Bardow, submitted to Industrial & Engineering Chemistry Research.

For questions, please contact Fabian Mayer: fmayer@ethz.ch

Follow these steps to run an isotherm fitting and extrapolation:
1. Put a csv-file with your isotherm data into the folder "input_data"
2. Specify your inputs in the file "main_fit_multicore.py" and run the file
3. Specify your inputs in the file "main_calc_and_extrapolate_isotherms.py" and run the file
