# ©2024 ETH Zurich, Fabian Mayer; D-MAVT; Energy and Process Systems Engineering

from scipy.optimize import minimize
import numpy as np

from feos.si import *

from calc_langmuir_isotherm import calc_langmuir_isotherms
from calc_toth_isotherm import calc_toth_isotherms
from calc_sips_isotherm import calc_sips_isotherms
from error_function import error_function
from read_optimization_input import read_initial_values, scale_var


def scipy_empirical_model_fit(input_isotherms, error_metric, isotherm_model="langmuir"):
    """
    Fit isotherm model parameters using scipy.optimize.minimize.

    Parameters
    ----------
    input_isotherms : list of input_isotherm
        List of Isotherm objects to which the model should be fitted.
    error_metric : str
        Error metric to be used for the fitting.
    isotherm_model : str
        Isotherm model to be used for the fitting.

    Returns
    -------
    optimized_isotherms : list of f'{isotherm_model}_isotherm'
        List of isotherm objects with fitted parameters.
    """


    # check if isotherm_model is valid
    if isotherm_model not in ["langmuir", "toth", "sips"]:
        raise ValueError("isotherm_model must be either 'langmuir', 'toth' or 'sips'")

    # user inputs
    path_bounds = f"optimization_input/bounds_{isotherm_model}.toml"
    path_initial_values = f"optimization_input/initial_values_{isotherm_model}.toml"
    # end user inputs

    # CALLBACK FUNCTION
    # objective function and constraints
    def callbackEvalF(x):
        """
        Callback function is called in each iteration to evaluate the black-box process.
        """

        print("Variables scaled:   ", x)

        try:
            x_unscaled = scale_var(x, path_bounds, unscale=True)
            print("Variables unscaled: ", x_unscaled)


            model_isotherms = eval(f'calc_{isotherm_model}_isotherms')(x_unscaled, input_isotherms)

            obj_fct_value, _ = error_function(input_isotherms, model_isotherms, error_metric=error_metric)

        except:
            # set objective function value to high value in case scaling or isotherm calculation was not successfull
            obj_fct_value = 1e4


        print("Objective function value: " + str(obj_fct_value))

        return obj_fct_value



    # Add variables
    #n = 3 if isotherm_model == "langmuir" else 5
    #remove alpha from optimization and set it to 0 because it is close to 0 anyways
    n = 3 if isotherm_model == "langmuir" else 4

    # Set upper and lower bounds to 0 and 1
    xLoBnds=[0.0 for i in range(n)]
    xUpBnds=[1.0 for i in range(n)]
    x_bounds = list(zip(xLoBnds, xUpBnds))


    # Set initial values 
    x_init_unscaled = read_initial_values(path_initial_values)
    # replace first entry of x_init_unscaled (parameter C) with maximum loading of input isotherms
    x_init_unscaled[0] = np.max([input_isotherm_i.max_loading/(MOL/KILOGRAM) for input_isotherm_i in input_isotherms])
    x_init = scale_var(x_init_unscaled, path_bounds)
        

    # Create callback
    result = minimize(callbackEvalF, x_init, bounds = x_bounds)

    # check if results are close to bounds of variables
    closetobounds_total = 0
    closetobounds = [False for i in range(n)]
    for i, x_i in enumerate(result.x):
        if x_i == 0 or x_i == 1:
            closetobounds[i] = True
            if isotherm_model not in ["sips", "toth"] or i != 4:
                closetobounds_total = 1
            

    x_unscaled = scale_var(result.x, path_bounds, unscale=True)

    return x_unscaled, result.fun, closetobounds_total, closetobounds





